<?php
// Block direct access to this file.
defined('ABSPATH') or die('No direct access allowed!');
/**
 * Our custom review post type
 */
class LexiconWpTest1CustomPost
{
    /**
     * LexiconWpTest1CustomPost constructor.
     *
     * Initializes the review post type
     */
    public function __construct()
    {
        add_action('init', array($this, 'registerCustomPostType'));
    }

    /**
     * Register function for custom post type.
     *
     * Registers review as a post type for wordpress.
     * with support for thumbnail and excerpt.
     */
    public function registerCustomPostType()
    {
        register_post_type('review', array(
            'labels'      => array('name'          => __('Reviews'),
                                   'singular_name' => __('Review')),
            'public'      => true,
            'has_archive' => true,
            'rewrite'     => array('slug' => 'review'),
            'supports'    => array('title', 'author', 'excerpt',
                                   'editor', 'thumbnail', 'revisions'),
        ));
    }
}
