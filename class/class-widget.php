<?php
// Block direct access to this file.
defined('ABSPATH') or die('No direct access allowed!');
/**
 * Widget for our custom post type
 */
class LexiconWpTest1Widget extends WP_Widget
{
    /**
     * LexiconWpTest1Widget constructor.
     *
     * Initializes the widget.
     */
    public function __construct()
    {
        wp_register_style('reviewCss',
            plugins_url(explode('/', plugin_basename(__FILE__))[0] . '/css/style-widget.css')
        );
        wp_enqueue_style('reviewCss');

        parent::__construct('review_widget', 'Review Widget', array(
            'classname'   => 'review_widget',
            'description' => 'Widget to show latest reviews',
        ));
    }

    /**
     * Shows the reviews on the widget.
     *
     * @param array $args The widget's arguments.
     * @param array $instance The widget's values from the database.
     */
    public function widget($args, $instance)
    {
        extract($args);
        $widgetTitle = apply_filters('widget_title', $instance['widgetTitle']);
        echo $before_widget; // wordpress before widget style
        if($widgetTitle) {
            echo $before_title . $widgetTitle . $after_title;
        }
        $this->getReviews($instance['numOfPosts']);
        echo $after_widget; // wordpress after widget style
    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance Previous settings from database.
     */
    public function form($instance)
    {
        /** @var string Should contain the title of the widget. */
        $widgetTitle = '';

        /** @var integer Should contain the amount of posts to show in the
         * widget. */
        $numOfPosts = '';
        if($instance) {
            $widgetTitle = esc_attr($instance['widgetTitle']);
            $numOfPosts = esc_attr($instance['numOfPosts']);
        }

        ?>
        <p>
            <label for="widgetTitle">
                <?php
                // For later use of WP's translate functions
                _e('Widget Title', 'review_widget');
                ?>
            </label>
            <input id="widgetTitle"
                   name="<?php echo $this->get_field_name('widgetTitle'); ?>"
                   type="text"
                   value="<?php echo $widgetTitle; ?>" />
        </p>
        <p>
            <label for="numOfPosts">
                <?php
                // For later use of WP's translate functions
                _e('Number of Posts', 'review_widget');
                ?>
            </label>
            <select id="numOfPosts"
                    name="<?php echo $this->get_field_name('numOfPosts')?>">
                <?php for($selNumPosts = 1; $selNumPosts <= 5; $selNumPosts++): ?>
                    <option
                        <?php echo $selNumPosts == $numOfPosts ?
                            'selected="selected"' :
                            ''; ?>
                        value="<?php echo $selNumPosts; ?>">
                        <?php echo $selNumPosts; ?>
                    </option>
                <?php endfor; ?>
            </select>
        </p>
        <?php
    }

    /**
     * Processing widget options on save, and stores them in the database
     *
     * @param array $new_instance The new options sent from user.
     * @param array $old_instance The previous options from database.
     *
     * @return array $instance The values to be saved to the database.
     */
    public function update($new_instance, $old_instance)
    {
        /** @var array Temporary save the old_instance in instance */
        $instance = $old_instance;

        /** @var string Save the title of the widget into the temporary
         * instance. */
        $instance['widgetTitle'] = strip_tags($new_instance['widgetTitle']);

        /** @var string Save the amount of posts to show into the temporary
         * instance. */
        $instance['numOfPosts'] = strip_tags($new_instance['numOfPosts']);

        // return the temporary instance.
        return $instance;
    }

    /**
     * Gets the reviews from wordpress database with WP_Query.
     * Resets the WP_Query settings to default after finished.
     *
     * @param integer $numOfPosts The number of posts to fetch.
     */
    public function getReviews($numOfPosts)
    {
        /** @var WP_Query Create a WP_Query to search for the custom post
         * type. */
        $reviews = new WP_Query(array(
            'posts_per_page' => $numOfPosts,
            'post_type'      => 'review',
            'orderby'        => 'date',
            'order'          => 'DESC',
        ));
        // Do we even have any posts to show
        if($reviews->have_posts()) {
            echo '<ul>';
            // while we still have posts to show, show them
            while($reviews->have_posts()) {
                $reviews->the_post(); // iteration function from WP
                ?>
                <a href="<?php echo get_permalink(); ?>">
                    <li class="widget_review_item">
                        <?php echo get_the_post_thumbnail(null, array(50, 50)); ?>
                        <p class="widget_review_title">
                            <?php echo get_the_title(); ?>
                        </p>
                        <p class="widget_review_excerpt">
                            <?php echo get_the_excerpt(); ?>
                        </p>
                        <p class="widget_review_author">
                            Author: <?php echo get_the_author(); ?>
                        </p>
                    </li>
                </a>
                <?php
            }
            echo '</ul>';
        } else {
            // We dont have any posts to show so show an error message
            echo '<p>No Reviews Found!</p>';
        }
        wp_reset_postdata();
    }
}
