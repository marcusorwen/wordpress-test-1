<?php
// Block direct access to this file.
defined('ABSPATH') or die('No direct access allowed!');
/*
Plugin Name: Lexicon IT-Konsult: WordPress Test 1
Description: WordPress Test 1
Author: Lexicon IT-Konsult
Version: 1.0
*/

require_once(plugin_dir_path(__FILE__) . 'class/class-review.php');
require_once(plugin_dir_path(__FILE__) . 'class/class-widget.php');

new LexiconWpTest1CustomPost();
new LexiconWpTest1Widget();

add_action('widgets_init', function(){
    register_widget('LexiconWpTest1Widget');
});